import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
private _user: Usuario;
private _token: string;

  constructor(private http: HttpClient) {
  }

  public get user(): Usuario {
    if (this._user != null) {
        return this._user;
    } else if(this._user == null && sessionStorage.getItem('usuario') != null) {
      this._user = JSON.parse(sessionStorage.getItem('usuario')) as Usuario; // Lo casteo a Usuario
      return this._user;
    }
    return new Usuario();
  }

  public get token(): string {
    if(this._token != null) { 
        return this._token;
    } else if(this._token == null && sessionStorage.getItem('token') != null) {
      this._token = (sessionStorage.getItem('token'));
      return this._token;
    } 
    return null;

  }

  login(user: Usuario): Observable<any> {
    const urlEndPoint = 'http://localhost:8080/oauth/token';
    const credentials = btoa('angularapp' + ':' + '12345');
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + credentials
    });

    let params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', user.username);
    params.set('password', user.password);

    return this.http.post<any>(urlEndPoint, params.toString(), {headers: httpHeaders});
  }

  saveUser(accessToken: string) : void {
    let payload = this.obtainTokenData(accessToken);
    this._user = new Usuario();
    this._user.firstname = payload.firstname;
    this._user.lastname = payload.firstname;
    this._user.email = payload.email;
    this._user.username = payload.user_name;
    this._user.roles = payload.authorities;

    sessionStorage.setItem('usuario', JSON.stringify(this._user)); // guardo datos en el sessionStorage
  }

  saveToken(accessToken: string) {
    this._token = accessToken;
    sessionStorage.setItem('token', accessToken);
  }

  obtainTokenData(accessToken: string) : any {
      if(accessToken != null) {
          return JSON.parse(atob(accessToken.split(".")[1]));
      } 
      return null;
  }

  isAuthenticated(): boolean {
    let payload = this.obtainTokenData(this.token);
    if((payload != null) && (payload.user_name) && (payload.user_name.length > 0)) {
      return true;
    }
    return false;
  }

  hasRole(role: string): boolean {
    let containRole = false;
    if(this.user.roles.includes(role)) {
      containRole = true;
    }
    return containRole;
  }

  logout() {
    this._token = null;
    this._user = null;
    //sessionStorage.clear(); //borra todas las variables del sessionStorage
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('usuario');
  }

}
