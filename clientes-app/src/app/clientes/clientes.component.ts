import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import {tap} from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { ModalService } from './detail/modal.service';
import { AuthService } from '../usuarios/auth.service';



@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
})
export class ClientesComponent implements OnInit {

  public clientes: Cliente[];
  private clienteService: ClienteService;
  private activateRoute: ActivatedRoute;
  private modalService: ModalService;
  public paginator: any;
  clienteSelected: Cliente;

  constructor(clienteService: ClienteService, activateRoute: ActivatedRoute, modalService: ModalService, public authService: AuthService) {
      this.clienteService = clienteService;
      this.activateRoute = activateRoute;
      this.modalService = modalService;
   }

  ngOnInit(): void {
    this.activateRoute.paramMap.subscribe ( params => {
      let page: number = + params.get('page'); /*Con el operador + casteo a number*/

      if(! page ) { /*Primera pagina*/
        page = 0;
      }
      this.clienteService.getClientes(page).pipe(
        tap(response  => {
          console.log('ClienteService: tap3');
          (response.content as Cliente[]).forEach( cliente => {
            console.log(cliente.firstname);
          });
        })
        ).subscribe(response =>  {
          this.clientes = response.content as Cliente[];
          this.paginator = response;
        });
      });

    this.modalService.notificateUpload.subscribe( cliente => {
        this.clientes = this.clientes.map(clienteOriginal => {
          if (cliente.id == clienteOriginal.id) {
            clienteOriginal.photo = cliente.photo;
          }
          return clienteOriginal;
        })
      });
  }

  delete(cliente: Cliente): void {
    Swal({
      title: 'Está seguro?',
      text: `¿Seguro que desea eliminar al cliente ${cliente.firstname} ${cliente.lastname}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.clienteService.delete(cliente.id).subscribe(
          response => {
            this.clientes = this.clientes.filter(cli => cli !== cliente)
            Swal.fire(
              'Cliente Eliminado!',
              `Cliente ${cliente.firstname} ${cliente.lastname} eliminado con éxito.`,
              'success'
            )
          }
        )

      }
    })
  }

  openModal(cliente: Cliente) {
    this.clienteSelected = cliente;
    this.modalService.openModal();
  }

}
