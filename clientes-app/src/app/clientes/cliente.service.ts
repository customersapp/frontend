import { Injectable } from '@angular/core';
import {formatDate} from '@angular/common';
import { Cliente } from './cliente';
import { Region} from './region';
import { Observable, throwError} from 'rxjs';
import {HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import {map, catchError, tap} from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class ClienteService {

  private httpClient: HttpClient;
  private urlCustomer: string = 'http://localhost:8080/api/1/customer';
  private urlRegion: string = 'http://localhost:8080/api/1/regions';
  //private httpHeaders: HttpHeaders;
  private router:Router;
  

  constructor(httpClient: HttpClient, router: Router) {
      this.httpClient = httpClient;
     // this.httpHeaders = new HttpHeaders({'Content-Type' : 'application/json'});
      this.router = router;
   }
/*
   private addAuthorizationHeader() {
    let token = this.authService.token;
    if(token != null ) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token) //Es inmutable, crea una nueva instancia de httpHeaders
    } 
    return this.httpHeaders;
   }
*/ // Lo reemplazo por un interceptor 

  getClientes(page: number): Observable<any> {
      //return this.httpClient.get<Cliente[]>(this.urlCustomer);
      return this.httpClient.get(this.urlCustomer + '/page/' + page).pipe(
        tap( (response: any) =>  {
          console.log('tap: 1');
          (response.content as Cliente[]).forEach( cliente => {
            console.log(cliente.firstname);
          });
        }),
        map(  (response: any) =>  {
          (response.content as Cliente[]).map(cliente => {
            cliente.firstname = cliente.firstname.toUpperCase();
            //cliente.createAt = formatDate(cliente.createAt, 'EEEE dd, MMMM, yyyy', 'es-AR'); ahora uso pipe en el html
            return cliente;
          });
          return response;
        }), 
        tap(response =>  {
          console.log('tap: 2');
          (response.content as Cliente[]).forEach( cliente => {
           console.log(cliente.firstname);
          });
        }),
      );
  }

  create(cliente: Cliente) : Observable<Cliente> {
    return this.httpClient.post<Cliente>(this.urlCustomer, cliente).pipe (
      catchError(e => {

        if(e.status == 400 ) {
            return throwError(e);
        }

        console.log(e.error.mensaje) /*e.error.campo a mostrar*/
        return throwError(e);
      })
    );
  }


  getCliente(id: number): Observable<Cliente> {
    return this.httpClient.get<Cliente>(`${this.urlCustomer}/${id}`).pipe(
      catchError( e => {
        if(e.status != 401 && e.error.mensaje) {
          this.router.navigate(['/clientes']);
          console.error(e.error.mensaje);
        }
        return throwError(e);
      })
    );
  }

  
  update(cliente: Cliente) : Observable<Cliente> {
    return this.httpClient.put<Cliente>(`${this.urlCustomer}`, cliente).pipe (
      catchError(e => {
        if(e.status == 400 ) {
          return throwError(e);
      }
        console.log(e.error.mensaje) /*e.error.campo a mostrar*/
        return throwError(e);
      })
    );
  }

  delete(id: number): Observable<Cliente> {
    return this.httpClient.delete<Cliente>(`${this.urlCustomer}/${id}`).pipe (
      catchError(e => {
        console.log(e.error.mensaje) /*e.error.campo a mostrar*/
        return throwError(e);
      })
    );
  }

  photoUpload(file: File, id): Observable<HttpEvent<{}>> {
    let formData = new FormData();
    formData.append("file", file);
    formData.append("id", id);
    const req = new HttpRequest('POST', `${this.urlCustomer}/upload`, formData, {
      reportProgress: true
    });

    return this.httpClient.request(req);
  }

  getRegiones(): Observable<Region[]> {
    return this.httpClient.get<Region[]>(this.urlRegion);
  }
}

