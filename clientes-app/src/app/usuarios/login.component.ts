import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { Usuario } from './usuario';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  titulo = "Por favor Inicie Sesion";
  usuario: Usuario;
  private authService: AuthService;
  private router: Router;

  constructor(authService: AuthService, router: Router) {
    this.usuario = new Usuario();
    this.authService = authService;
    this.router = router;
  }

  ngOnInit(): void {
    if(this.authService.isAuthenticated()) {
      swal('Login', `Hola ${this.authService.user.username} ya estas autenticado!`, 'info' );
      this.router.navigate(['/clientes']);
    }
  }

  login() : void {
    console.log(this.usuario);
    if(this.usuario.username == null || this.usuario.password == null) {
      swal('Error Login', 'Username o Password vacias!', 'error');
    }
    this.authService.login(this.usuario).subscribe( response => {
      console.log(response);

        this.authService.saveUser(response.access_token);
        this.authService.saveToken(response.access_token);

        let usuario = this.authService.user;
        
        this.router.navigate(['/clientes']);

        swal('Login', `Hola ${usuario.username} , has iniciado sesion con exito!`, 'success');
      },
      error => {
        if(error.status == 400) {
          swal('Error Login', 'Username o Password incorrectas!', 'error');
        }
      }
    );
  }
}
