import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  modal: boolean = false;
  private _notificateUpload = new EventEmitter<any>();

  constructor() {
  }


  get notificateUpload(): EventEmitter<any> {
    return this._notificateUpload;
  }

  openModal() {
    this.modal = true;
  }

  closeModal() {
    this.modal = false;
  }
}
