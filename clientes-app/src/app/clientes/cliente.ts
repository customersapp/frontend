import { Region } from './region';

export class Cliente {
    id: number;
    firstname: string;
    lastname: string;
    email: string;
    createAt: string;
    photo: string;
    region: Region;
}
