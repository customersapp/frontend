import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { AuthService } from '../usuarios/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public authService: AuthService, private router: Router) {

   }

  ngOnInit(): void {
  }

  logout(): void {
    let username = this.authService.user.username;
    this.authService.logout();
    swal('Logout', `Hola ${username} , has cerrado sesion con exito!`, 'success');
    this.router.navigate(['/login']);
  }

}
