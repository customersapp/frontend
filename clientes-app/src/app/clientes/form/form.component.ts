import { Component, OnInit } from '@angular/core';
import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';
import {Router, ActivatedRoute} from '@angular/router';
import swal from 'sweetalert2';
import { Region } from '../region';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent implements OnInit {

  public cliente: Cliente;
  regiones: Region[];
  public title: string;
  private clienteService: ClienteService;
  private router: Router;
  private activateRoute: ActivatedRoute;
  public errors: string[];

  constructor(clienteService: ClienteService, router:Router, activateRoute: ActivatedRoute) {
    this.cliente = new Cliente();
    this.title = "Crear cliente";
    this.clienteService = clienteService;
    this.router = router;
    this.activateRoute = activateRoute;
   }

  ngOnInit(): void {
    this.cargarCliente();
  }


  public create(): void {
    this.clienteService.create(this.cliente)
    .subscribe(cliente => {
        this.router.navigate(['/clientes']);
       swal('Nuevo Cliente', `Cliente ${cliente.firstname} ${cliente.lastname} creado con exito!`, 'success');
      },
      err => {
        this.errors = err.error.errors as string[];
        console.log(err.error.errors);
        console.log(err.status);
      }
    );
  }

  public cargarCliente(): void {
    this.activateRoute.params.subscribe(params => {
      let id = params['id'];
      if(id) {
        this.clienteService.getCliente(id).subscribe( (cliente) => this.cliente = cliente);
      }
    });

    this.clienteService.getRegiones().subscribe(regiones => this.regiones = regiones);
  }

  public update(): void {
    this.clienteService.update(this.cliente).subscribe( cliente => {
      this.router.navigate(['/clientes'])
      swal('Cliente Actualizado', `Cliente ${cliente.firstname} ${cliente.lastname} actualizado con exito!`, 'success');
    }, /* 'si hay error en el suscribe, vamos por err' */
    err => {
      this.errors = err.error.errors as string[];
      console.log(err.error.errors);
      console.log(err.status);
    })
  }


  compareRegion(iteracion: Region, asignado: Region) : boolean {

    if(iteracion === undefined && asignado === undefined) {
      return true;
    }

    return iteracion == null || asignado == null ? false : iteracion.id === asignado.id;
  }
} 
