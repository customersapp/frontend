import { Component, OnInit, Input } from '@angular/core';
import { Cliente } from '../cliente';
import {ClienteService} from '../cliente.service';
import swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { ModalService } from './modal.service';
import { AuthService } from 'src/app/usuarios/auth.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

@Input() cliente: Cliente;

private clienteService: ClienteService;
title: string ="Detalle del cliente";
imageSelected: File;
progress: number = 0;
modalService: ModalService;

  constructor(clienteService: ClienteService, modalService: ModalService, public authService: AuthService) {
    this.clienteService = clienteService;
    this.modalService = modalService;
   }

  ngOnInit(): void {}

  selectPhoto(event) {
    this.imageSelected = event.target.files[0];
    this.progress = 0;
    console.log(this.imageSelected);
    if(this.imageSelected.type.indexOf('image') < 0) {
      swal('Error seleccionar imagen: ', 'El archivo debe ser de tipo imagen', 'error');
      this.imageSelected = null;
    }
  }

  uploadPhoto() {

    if(! this.imageSelected) {
      swal('Error Upload: ', 'Debe seleccionar una foto', 'error');
    } else {
    this.clienteService.photoUpload(this.imageSelected, this.cliente.id)
    .subscribe( event => {
      
      if(event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round((event.loaded / event.total) * 100);
      } else if (event.type === HttpEventType.Response) {
        let response: any = event.body; /*Body me llega mensaje y customer*/
        this.cliente = response.customer as Cliente;

        this.modalService.notificateUpload.emit(this.cliente);

        swal('La foto se ha subido completamente!', 'La foto se ha subido con exito!', 'success');
      }
    });
  }
}

  closeModal() {
  this.modalService.closeModal();
  this.imageSelected = null;
  this.progress = 0;
  }
}
