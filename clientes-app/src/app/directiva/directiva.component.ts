import { Component} from '@angular/core';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html',
})
export class DirectivaComponent  {

  constructor() { }

  listaCurso: string[] = ['TypeScript', 'JavaScript', 'JAVA SE', 'C#', 'PHP']
  habilitar: boolean = true;

  public setHabilitar(): void {
    this.habilitar = (this.habilitar == true)? false : true;
  }
}
